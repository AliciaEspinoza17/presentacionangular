import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { Component2Component } from './component/component2/component2.component';
import { Component3Component } from './component/component3/component3.component';
import { Component4Component } from './component/component4/component4.component';
import { Component5Component } from './component/component5/component5.component';
import { Component6Component } from './component/component6/component6.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    Component2Component,
    Component3Component,
    Component4Component,
    Component5Component,
    Component6Component
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
